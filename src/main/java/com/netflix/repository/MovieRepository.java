package com.netflix.repository;

import com.netflix.dto.MovieDto;

import java.util.List;

public interface MovieRepository {
    public List<MovieDto> getMovies();
}
