package com.netflix.service;

import com.netflix.dto.MovieDto;
import com.netflix.repository.MovieRepositoryImpl;

import java.util.List;

public interface MovieService{
    public List<MovieDto> getAllMovies();
}
