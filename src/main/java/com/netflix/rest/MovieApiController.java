package com.netflix.rest;

import com.netflix.dto.MovieDto;
import com.netflix.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.netflix.repository.MovieRepositoryImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MovieApiController {
    public MovieApiController() {
    }

    @Autowired
    private MovieService movieService;

    @CrossOrigin
    @RequestMapping(value = "/movies", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,List<MovieDto>>> getAllMovies(){
        final List<MovieDto> movieDtoList = movieService.getAllMovies();
        Map<String, List<MovieDto>> mapMovies = new HashMap<>();
        mapMovies.put("movies", movieDtoList);
        return ResponseEntity.ok().body(mapMovies);
    }
}
