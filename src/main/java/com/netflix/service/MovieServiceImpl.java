package com.netflix.service;

import com.netflix.dto.MovieDto;
import com.netflix.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<MovieDto> getAllMovies() {
        return movieRepository.getMovies();
    }
}
