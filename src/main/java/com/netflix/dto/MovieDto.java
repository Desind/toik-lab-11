package com.netflix.dto;

public class MovieDto {
    int movieId;
    String title;
    int year;
    String image;

    public MovieDto(int id, String title, int year, String photo) {
        this.movieId = id;
        this.title = title;
        this.year = year;
        this.image = photo;
    }

    public int getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "{" +
                "movieId=" + movieId +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", image='" + image + '\'' +
                '}';
    }
}
